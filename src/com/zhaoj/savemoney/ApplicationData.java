package com.zhaoj.savemoney;

import java.util.Map;

import android.R.integer;
import android.app.Activity;
import android.content.Context;

import cn.kuaipan.android.openapi.KuaipanAPI;

import com.zhaoj.savemoney.database.AccountDBHelper;
import com.zhaoj.savemoney.database.AccountManagerDBHelper;
import com.zhaoj.savemoney.utils.PreferencesUtils;
import com.zhaoj.savemoney.widget.CostTypeItem;

public class ApplicationData {

	// preferences 设置的数据
	public static PreferencesUtils preferencesData;
	public static boolean bVoiceRecog;
	// sqllite 全局变量
	public static AccountDBHelper accountDBHelper;
	public static AccountManagerDBHelper accountManagerDBHelper;
	// 工程中用到的全局数据
	public static Map<integer, CostTypeItem> CostTypeItems;
	public static Context MainActivitycontext;
	public static Activity mainActivity;
	public static Activity settingActivity;
	public static int VersionCode;
	public static int accountID;
	// 快盘相关
	public static KuaipanAPI mApi;
	public static int authType;
}
