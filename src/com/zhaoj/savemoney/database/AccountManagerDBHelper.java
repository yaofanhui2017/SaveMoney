package com.zhaoj.savemoney.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.utils.ConstantsUtils;

public class AccountManagerDBHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "accountmanger";
	private static final int DATABASE_VERSION = 2;
	private final static String DEFAULT_DBNAME = "savemoney";
	private final static String DEFAULT_DBDESCRIPTION = "Ĭ���˱�";

	public AccountManagerDBHelper(Context context) {
		super(context, ConstantsUtils.getDBFileName(DATABASE_NAME), null,
				DATABASE_VERSION);
	}

	public AccountManagerDBHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@SuppressLint("NewApi")
	public AccountManagerDBHelper(Context context, String name,
			CursorFactory factory, int version,
			DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createDBSchma(db);
		insertDefaultAccountRecord(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion == 1) {
			updateTablesForV2(db, oldVersion, newVersion);
		}
	}

	private void updateTablesForV2(SQLiteDatabase db, int oldVersion,
			int newVersion) {
		try {
			db.beginTransaction();

			createDBSchma(db);

			Cursor cursor = db.rawQuery("select * from account_tbl", null);
			if (cursor.moveToFirst()) {

				int accountManagerIDIndex = cursor.getColumnIndex("account_id");
				int nameIndex = cursor.getColumnIndex("account_name");
				int pathIndex = cursor.getColumnIndex("account_path");
				int isShareIndex = cursor.getColumnIndex("account_isshare");

				do {
					ContentValues values = new ContentValues();
					values.put(
							SaveMoneyData.AccountManagerColumns.ACOUNT_MANAGER_ID,
							cursor.getInt(accountManagerIDIndex));
					values.put(SaveMoneyData.AccountManagerColumns.NAME,
							cursor.getString(nameIndex));
					values.put(SaveMoneyData.AccountManagerColumns.URI, Uri
							.parse(cursor.getString(pathIndex)).toString());
					values.put(SaveMoneyData.AccountManagerColumns.IS_SHARE,
							cursor.getInt(isShareIndex));
					values.put(
							SaveMoneyData.AccountManagerColumns.VERSION_CODE,
							AccountDBHelper.DATABASE_VERSION);
					if (ApplicationData.accountID == cursor
							.getInt(accountManagerIDIndex)) {
						values.put(
								SaveMoneyData.AccountManagerColumns.IS_SELECTED,
								1);
					} else {
						values.put(
								SaveMoneyData.AccountManagerColumns.IS_SELECTED,
								0);
					}

					db.insert(SaveMoneyData.AccountManagerColumns.TABLE_NAME,
							null, values);
				} while (cursor.moveToNext());
			}

			db.execSQL("drop table if exists account_tbl");

			db.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
	}

	private void createDBSchma(SQLiteDatabase db) {
		db.execSQL(formatCreateTableSql(
				SaveMoneyData.AccountManagerColumns.TABLE_NAME,
				SaveMoneyData.AccountManagerColumns.COLUMNS));
	}

	public void insertDefaultAccountRecord(SQLiteDatabase db) {
		ContentValues values = new ContentValues();

		values.put(SaveMoneyData.AccountManagerColumns.NAME,
				DEFAULT_DBDESCRIPTION);
		values.put(SaveMoneyData.AccountManagerColumns.URI,
				Uri.parse(ConstantsUtils.getDBFileName(DEFAULT_DBNAME))
						.toString());
		values.put(SaveMoneyData.AccountManagerColumns.IS_SHARE,
				SaveMoneyData.BOOLEAN_FALSE);
		values.put(SaveMoneyData.AccountManagerColumns.IS_SELECTED,
				SaveMoneyData.BOOLEAN_TRUE);
		values.put(SaveMoneyData.AccountManagerColumns.VERSION_CODE,
				AccountDBHelper.DATABASE_VERSION);

		db.insert(SaveMoneyData.AccountManagerColumns.TABLE_NAME, null, values);
	}

	private String formatCreateTableSql(String tableName, String[][] columns) {
		if (tableName == null || columns == null || columns.length == 0) {
			throw new IllegalArgumentException(
					"Invalid parameters for creating table " + tableName);
		} else {
			StringBuilder stringBuilder = new StringBuilder("CREATE TABLE ");
			stringBuilder.append(tableName);
			stringBuilder.append(" (");
			for (int n = 0, i = columns.length; n < i; n++) {
				if (n > 0) {
					stringBuilder.append(", ");
				}
				stringBuilder.append(columns[n][0]).append(" ")
						.append(columns[n][1]);
			}
			return stringBuilder.append(");").toString();
		}
	}
}
