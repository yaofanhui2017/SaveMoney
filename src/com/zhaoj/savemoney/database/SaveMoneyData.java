/**   
 * SaveMoney
 *
 * @Date     2014-8-23
 * @author   zhaojie 
 *
 **/
package com.zhaoj.savemoney.database;

import android.R.integer;
import android.R.string;
import android.net.Uri;
import android.provider.BaseColumns;

public class SaveMoneyData {
	public static final String CONTENT = "content://";
	public static final String AUTHORITY = "com.zhaoj.savemoney.provider.data";
	public static final String CONTENT_AUTHORITY = CONTENT + AUTHORITY;

	static final String AUTOINCREMENT = " AUTOINCREMENT";
	static final String REFERENCES = " REFERENCES";

	static final String TYPE_PRIMARY_KEY = "INTEGER PRIMARY KEY AUTOINCREMENT";

	static final String TYPE_EXTERNAL_ID = "INTEGER(7)";
	static final String TYPE_TEXT = "TEXT";
	static final String TYPE_DATE = "DATE";
	static final String TYPE_DOUBLE = "DOUBLE";
	static final String TYPE_TEXT_UNIQUE = "TEXT UNIQUE";
	static final String TYPE_INT = "INT";
	static final String TYPE_BOOLEAN = "INTEGER(1)";

	static final int BOOLEAN_FALSE = 0;
	static final int BOOLEAN_TRUE = 1;

	public static class CostTypeColumns implements BaseColumns {
		public static final String TABLE_NAME = "costType";

		public static final String COSTTYPE_ID = "costTypeID";
		public static final String NAME = "name";
		public static final String ICON_ID = "iconID";
		public static final String KIND = "kind";

		public static final Uri CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + "/"
				+ TABLE_NAME);

		public static Uri CONTENT_URI(int id) {
			return Uri.parse(CONTENT_AUTHORITY + "/" + TABLE_NAME + "/" + id);
		}

		public static final String PROJECTIONS[] = new String[] { _ID,
				COSTTYPE_ID, NAME, ICON_ID, KIND };

		public static final String COLUMNS[][] = { { _ID, TYPE_PRIMARY_KEY },
				{ COSTTYPE_ID, TYPE_INT + AUTOINCREMENT }, { NAME, TYPE_TEXT },
				{ ICON_ID, TYPE_INT }, { KIND, TYPE_BOOLEAN } };
	}

	public static class AcountColumns implements BaseColumns {
		public static final String TABLE_NAME = "acount";

		public static final String ACOUNT_ID = "acountID";
		public static final String TIME = "time";
		public static final String AMOUNT = "amount";
		public static final String COST_TYPE = "costType";
		public static final String REMRAK = "remark";
		public static final String KIND = "kind";

		public static final Uri CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + "/"
				+ TABLE_NAME);

		public static Uri CONTENT_URI(int id) {

			return Uri.parse(CONTENT_AUTHORITY + "/" + TABLE_NAME + "/" + id);
		}

		public static final String PROJECTION[] = { _ID, ACOUNT_ID, TIME,
				AMOUNT, COST_TYPE, REMRAK, KIND };

		public static final String COLUMNS[][] = {
				{ _ID, TYPE_PRIMARY_KEY },
				{ ACOUNT_ID, TYPE_INT + AUTOINCREMENT },
				{ TIME, TYPE_DATE },
				{ AMOUNT, TYPE_DOUBLE },
				{
						COST_TYPE,
						TYPE_INT + REFERENCES + " "
								+ CostTypeColumns.TABLE_NAME + "("
								+ CostTypeColumns.COSTTYPE_ID + ")" },
				{ REMRAK, TYPE_TEXT }, { KIND, TYPE_BOOLEAN } };
	}

	public static class AccountManagerColumns implements BaseColumns {
		public static final String TABLE_NAME = "accountManager";

		public static final String ACOUNT_MANAGER_ID = "accountManagerID";
		public static final String NAME = "name";
		public static final String URI = "uri";
		public static final String IS_SHARE = "isShare";
		public static final String IS_SELECTED = "isSelected";
		public static final String VERSION_CODE = "versionCode";

		public static final Uri CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + "/"
				+ TABLE_NAME);

		public static Uri CONTENT_URI(int id) {
			return Uri.parse(CONTENT_AUTHORITY + "/" + TABLE_NAME + "/" + id);
		}

		public static final String PROJECTION[] = { _ID, ACOUNT_MANAGER_ID,
				NAME, URI, IS_SHARE, IS_SELECTED, VERSION_CODE };

		public static final String COLUMNS[][] = { { _ID, TYPE_PRIMARY_KEY },
				{ ACOUNT_MANAGER_ID, TYPE_INT + AUTOINCREMENT },
				{ NAME, TYPE_TEXT }, { URI, TYPE_TEXT },
				{ IS_SHARE, TYPE_BOOLEAN }, { IS_SELECTED, TYPE_BOOLEAN },
				{ VERSION_CODE, TYPE_INT } };
	}

}
