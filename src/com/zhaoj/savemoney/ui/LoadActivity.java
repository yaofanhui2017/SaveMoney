package com.zhaoj.savemoney.ui;

import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.service.InitAppService;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;

public class LoadActivity extends Activity {

	private static final int LOAD_DISPLAY_TIME = 1500;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFormat(PixelFormat.RGBA_8888);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DITHER);

		setContentView(R.layout.activity_load);
		startInitAppService(null);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent mainIntent = new Intent(LoadActivity.this,
						MainActivity.class);
				LoadActivity.this.startActivity(mainIntent);
				LoadActivity.this.finish();
			}
		}, LOAD_DISPLAY_TIME);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.load, menu);
		return true;
	}

	private void startInitAppService(View source) {
		Intent intent = new Intent(this, InitAppService.class);
		startService(intent);
	}

}
