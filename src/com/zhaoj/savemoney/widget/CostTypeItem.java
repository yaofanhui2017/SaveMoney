package com.zhaoj.savemoney.widget;


public class CostTypeItem {
	
	private int mCostTypeID;
	private String mCostTypeName;
	private int mCostTypeIconID;
	
	public CostTypeItem() {
		// TODO Auto-generated constructor stub
	}
	
	public CostTypeItem(int costTypeID, String CostTypeName, int CostTypeIconID){
		mCostTypeID = costTypeID;
		mCostTypeName = CostTypeName;
		mCostTypeIconID = CostTypeIconID;
	}
	public int getmCostTypeID() {
		return mCostTypeID;
	}

	public void setmCostTypeID(int mCostTypeID) {
		this.mCostTypeID = mCostTypeID;
	}

	public String getmCostTypeName() {
		return mCostTypeName;
	}

	public void setmCostTypeName(String mCostTypeName) {
		this.mCostTypeName = mCostTypeName;
	}

	public int getmCostTypeIconID() {
		return mCostTypeIconID;
	}

	public void setmCostTypeIconID(int mCostTypeIconID) {
		this.mCostTypeIconID = mCostTypeIconID;
	}

}
