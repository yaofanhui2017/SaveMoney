package com.zhaoj.savemoney.widget;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.zhaoj.savemoney.R;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 功能描述：自定义顶部菜单栏
 * 
 * @author android_ls
 */
@SuppressLint("SimpleDateFormat")
public class DateMenuBar extends FrameLayout {

	private static final String DEFAOUT_DATEFORMAT = "yyyy年MM月dd E";

	private String dateFormatString;
	private SimpleDateFormat dateFormat;
	private Context mContext;

	public LinearLayout llDateBar;
	public Calendar curCalendar;
	public String dateString;
	public TextView tvDateTitle;
	public ImageButton leftButton;
	public ImageButton rightButton;
	public ImageButton selectDateButton;

	{
		dateFormat = new SimpleDateFormat();
		curCalendar = Calendar.getInstance();
	}

	private OnMenuClickListener mOnClickListener;
	private OnDateStringChangedListener mOnDateChangedListener;

	public DateMenuBar(Context context) {
		super(context);
		setupViews();
		mContext = context;
	}

	public DateMenuBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		setupViews();
		mContext = context;
	}

	public void setOnClickListener(OnMenuClickListener onClickListener) {
		mOnClickListener = onClickListener;
	}

	private void setupViews() {

		final LayoutInflater mLayoutInflater = LayoutInflater
				.from(getContext());
		LinearLayout rlTopNavbar = (LinearLayout) mLayoutInflater.inflate(
				R.layout.view_datemenu, null);
		addView(rlTopNavbar);

		llDateBar = (LinearLayout) rlTopNavbar
				.findViewById(R.id.datemenulayout);

		tvDateTitle = (TextView) rlTopNavbar.findViewById(R.id.tv_datetitle);
		dateFormat.applyPattern(getDateFormatString());
		dateString = dateFormat.format(curCalendar.getTime());
		tvDateTitle.setText(dateString);

		rightButton = (ImageButton) rlTopNavbar
				.findViewById(R.id.right_datebutton);
		leftButton = (ImageButton) rlTopNavbar
				.findViewById(R.id.left_datebutton);

		selectDateButton = (ImageButton) rlTopNavbar
				.findViewById(R.id.middle_selectbutton);
		selectDateButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DatePickerDialog(mContext,
						new DatePickerDialog.OnDateSetListener() {

							@Override
							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {
								curCalendar.set(year, monthOfYear, dayOfMonth);
								ReFreshDateFormat();
							}
						}, curCalendar.get(Calendar.YEAR), curCalendar
								.get(Calendar.MONTH), curCalendar
								.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

		llDateBar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mOnClickListener != null) {
					mOnClickListener.onClick();
				}
			}
		});

	}

	public String getDateFormatString() {
		if (dateFormatString == "" || dateFormatString == null)
			dateFormatString = DEFAOUT_DATEFORMAT;
		return dateFormatString;
	}

	public void setDateFormatString(String dateFormatString) {
		this.dateFormatString = dateFormatString;
		ReFreshDateFormat();
	}

	public void ReFreshDateFormat() {
		dateFormat.applyPattern(getDateFormatString());
		dateString = dateFormat.format(curCalendar.getTime());
		tvDateTitle.setText(dateString);
		if (mOnDateChangedListener != null) {
			mOnDateChangedListener.onDateStringChanged();
		}
	}

	public OnDateStringChangedListener getmOnDateChangedListener() {
		return mOnDateChangedListener;
	}

	public void setmOnDateChangedListener(
			OnDateStringChangedListener mOnDateChangedListener) {
		this.mOnDateChangedListener = mOnDateChangedListener;
	}

	public interface OnMenuClickListener {
		public abstract void onClick();
	}

	public interface OnDateStringChangedListener {
		public abstract void onDateStringChanged();
	}
}