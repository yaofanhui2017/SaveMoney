package com.zhaoj.savemoney.widget;

import java.util.ArrayList;
import java.util.List;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.utils.ConstantsUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DetailExListViewAdapter extends BaseExpandableListAdapter {
	private static final String QUERY_DETAIL_SQL_STRING = "select * from default_account_tbl "
			+ "where (defacu_kind = ?) and (datetime(defacu_time) = datetime(?))";
	private static final String QUERY_DETAIL_ALL_SQL_STRING = "select * from default_account_tbl "
			+ "where datetime(defacu_time) = datetime(?)";
	private static final String QUERY_COSTTYPE_SQL_STRING = "select * from costtype_tbl where costtype_id = ?";

	private Context mContext;
	private boolean mIsShowRemark;
	private String mDateString;
	private List<DetailListItem> mListItems;

	public DetailExListViewAdapter(Context context,
			List<DetailListItem> listItems) {
		mContext = context;
		mListItems = listItems;
	}

	public DetailExListViewAdapter(Context context, int costKind,
			boolean isShowRemark, String dateString) {
		mContext = context;
		mListItems = new ArrayList<DetailListItem>();
		mIsShowRemark = isShowRemark;
		mDateString = dateString;
		InitListItemsWithoutGroup(costKind);
	}

	public void ReFresh(int costKind, boolean isShwoRemark, String dateString) {
		mIsShowRemark = isShwoRemark;
		mDateString = dateString;
		InitListItemsWithoutGroup(costKind);
		notifyDataSetChanged();
		notifyDataSetInvalidated();
	}

	private void InitListItemsWithoutGroup(int costKind) {
		mListItems.clear();
		Cursor cursor;
		if (costKind == ConstantsUtils.COST_KIND_ALL) {
			cursor = ApplicationData.accountDBHelper.getReadableDatabase()
					.rawQuery(QUERY_DETAIL_ALL_SQL_STRING, new String[] {mDateString});
		} else {
			cursor = ApplicationData.accountDBHelper.getReadableDatabase()
					.rawQuery(
							QUERY_DETAIL_SQL_STRING,
							new String[] { Integer.toString(costKind),
									mDateString });
		}

		addGroupWithoutGroup(cursor);
	}

	private void addGroupWithoutGroup(Cursor cursor) {
		int i = 0;
		DetailListItem listItem = new DetailListItem();
		listItem.setID(0);
		listItem.setName(" ");
		listItem.setRemark(" ");
		// 组没有操作指示图标
		// listItem.setDrawableId(drawableId);

		Cursor costtypeCursor;
		ArrayList<DetailListItem> DetailItems = new ArrayList<DetailListItem>();

		try {
			while (cursor.moveToNext()) {
				DetailListItem detailListItem = new DetailListItem();
				detailListItem.setID(i++);
				detailListItem.setAmount(cursor.getDouble(cursor
						.getColumnIndex("defacu_amount")));
				detailListItem.setRemark(cursor.getString(cursor
						.getColumnIndex("defacu_remark")));
				costtypeCursor = ApplicationData.accountDBHelper
						.getReadableDatabase()
						.rawQuery(
								QUERY_COSTTYPE_SQL_STRING,
								new String[] { Integer.toString(cursor.getInt(cursor
										.getColumnIndex("defacu_costtype"))) });
				while (costtypeCursor.moveToNext()) {
					detailListItem.setDrawableID(costtypeCursor
							.getInt(costtypeCursor
									.getColumnIndex("costtype_icon_id")));
					detailListItem.setName(costtypeCursor
							.getString(costtypeCursor
									.getColumnIndex("costtype_name")));
				}
				DetailItems.add(detailListItem);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		listItem.setGroups(DetailItems);
		mListItems.add(listItem);
	}

	@SuppressWarnings("unused")
	private void addGroupWithGroup(int groupId, String[] groupNames,
			int[] groupicons) {
		DetailListItem listItem = new DetailListItem();
		listItem.setID(groupId);
		listItem.setName(groupNames[groupId]);
		// 组没有操作指示图标
		// listItem.setDrawableId(drawableId);

		ArrayList<DetailListItem> firstGroup = new ArrayList<DetailListItem>();
		for (int i = 0; i < groupNames.length; i++) {
			DetailListItem firstGroupItem = new DetailListItem();
			firstGroupItem.setID(i);
			firstGroupItem.setName(groupNames[i]);
			firstGroupItem.setDrawableID(groupicons[i]);

			// 可以无限延伸
			// firstGroupItem.setGroups(null);
			firstGroup.add(firstGroupItem);
		}

		listItem.setGroups(firstGroup);
		mListItems.add(listItem);
	}

	public DetailListItem getChild(int groupPosition, int childPosition) {
		return mListItems.get(groupPosition).getGroups().get(childPosition);
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = getItemLayout();

			viewHolder = new ViewHolder();
			viewHolder.costTypeNameTextView = (TextView) convertView
					.findViewById(R.id.costtype_name);
			viewHolder.amountTextView = (TextView) convertView
					.findViewById(R.id.amount);
			viewHolder.costTypeIconImageView = (ImageView) convertView
					.findViewById(R.id.costtype_icon);
			viewHolder.remarkTextView = (TextView) convertView
					.findViewById(R.id.remark);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		DetailListItem listItem = getChild(groupPosition, childPosition);
		viewHolder.costTypeIconImageView.setBackgroundResource(listItem
				.getDrawableID());
		viewHolder.costTypeNameTextView.setText(listItem.getName());
		viewHolder.remarkTextView.setText(listItem.getRemark());

		if (listItem.getAmount() > 0) {
			viewHolder.amountTextView.setTextColor(Color.GREEN);
		} else {
			viewHolder.amountTextView.setTextColor(Color.RED);
		}
		viewHolder.amountTextView.setText("￥"
				+ Double.toString(listItem.getAmount()));

		if (mIsShowRemark) {
			viewHolder.remarkTextView.setVisibility(View.VISIBLE);
		} else {
			viewHolder.remarkTextView.setVisibility(View.GONE);
		}
		return convertView;
	}

	public int getChildrenCount(int groupPosition) {
		return mListItems.get(groupPosition).getGroups().size();
	}

	public DetailListItem getGroup(int groupPosition) {
		return mListItems.get(groupPosition);
	}

	public int getGroupCount() {
		return mListItems.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = getItemLayout();

			viewHolder = new ViewHolder();
			viewHolder.costTypeNameTextView = (TextView) convertView
					.findViewById(R.id.costtype_name);
			viewHolder.remarkTextView = (TextView) convertView
					.findViewById(R.id.remark);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.costTypeNameTextView.setText(getGroup(groupPosition)
				.getName());
		viewHolder.remarkTextView.setVisibility(View.GONE);
		return convertView;
	}

	public boolean hasStableIds() {
		return false;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	static class ViewHolder {
		public ImageView costTypeIconImageView;
		public TextView costTypeNameTextView;
		public TextView amountTextView;
		public TextView remarkTextView;
	}

	private LinearLayout getItemLayout() {
		LayoutInflater layoutInflater = LayoutInflater.from(mContext);
		LinearLayout layout = (LinearLayout) layoutInflater.inflate(
				R.layout.layout_detailitem, null);
		return layout;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

}
